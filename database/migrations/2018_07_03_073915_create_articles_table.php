<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string( 'link')->nullable();
            $table->string( 'title')->nullable();
            $table->string( 'image')->nullable();
            $table->string('type')->nullable();
	        $table->text( 'description')->nullable();
            $table->string( 'og_title')->nullable();
            $table->string( 'og_image')->nullable();
            $table->string( 'og_description')->nullable();
            $table->text( 'content')->nullable();
            $table->integer( 'portal_id')->nullable();
	        $table->boolean( 'in_feed')->nullable();
	        $table->boolean('processed')->nullable();
	        $table->dateTime( 'processed_when')->nullable();
	        $table->dateTime( 'published_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
