<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->integer( 'fb_reactions')->nullable();
            $table->integer( 'fb_comments')->nullable();
            $table->integer( 'fb_shares')->nullable();
            $table->float('virality_absolute')->nullable();
            $table->integer( 'virality_normalized')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn( ['fb_reactions', 'fb_comments', 'fb_shares', 'virality_absolute', 'virality_normalized']);
        });
    }
}
