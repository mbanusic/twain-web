<?php

use Illuminate\Database\Seeder;

class SourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('sources')->insert([
		    ['url' => 'http://www.wsj.com/xml/rss/3_7085.xml', 'type' => 'rss', 'portal_id' => 1],
	    ]);
    }
}
