<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('users')->insert([
	    	['email' => 'mbanusic@gmail.com', 'name' => 'Marko', 'password' => bcrypt( 'secret123')],
	    	['email' => 'velemiran@gmail.com', 'name' => 'Miran', 'password' => bcrypt( 'secret123')]
	    ]);
    }
}
