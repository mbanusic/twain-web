<?php

Route::get('newsfeed/auto-main-feed', 'OldApiController@main_feed');
Route::get('categories', 'OldApiController@categories');
Route::get('categories/auto-category-feed', 'OldApiController@category');
Route::get('search', 'OldApiController@search');
Route::get('topics/{topic}', 'OldApiController@topic');
Route::get('topics', 'OldApiController@topics');
Route::post('firebase/create', 'OldApiController@main_feed');