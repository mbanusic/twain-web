<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('search', 'HomeController@search')->name( 'search');

Route::group(['prefix' => 'portals/{portal}'], function() {
	Route::resource( 'xpaths', 'ContentXpathController');
	Route::resource( 'sources', 'SourceController');
});

Route::resource( 'articles', 'ArticleController');
Route::resource( 'portals', 'PortalController');
Route::resource( 'topics', 'TopicController');
Route::resource( 'categories', 'CategoryController');
Route::resource( 'category_regex', 'RegexController');



