<?php

namespace App\Console\Commands;

use App\DatabaseModels\Portal;
use App\DatabaseModels\Source;
use Illuminate\Console\Command;

class LoadHomepages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twain:homepages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $portals = Portal::all();
        foreach($portals as $portal) {
        	if (!$portal->sources()->where('type', 'homepage')->count()) {
        		$portal->sources()->save(new Source(['type' => 'homepage', 'url' => $portal->url]));
	        }
        }
    }
}
