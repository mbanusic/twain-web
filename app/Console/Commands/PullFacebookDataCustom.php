<?php

namespace App\Console\Commands;

use App\DatabaseModels\Article;
use Carbon\Carbon;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Console\Command;

class PullFacebookDataCustom extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twain:pull:facebook {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        switch ($type) {
            case 'old':
                $this->oldData();
                break;
            case 'recent':
                $this->recentData();
                break;
            case 'new':
                $this->newData();
                break;
        }
    }

    private function newData() {
        for ($i=0;$i<50;$i++) {
            $articles = Article::where('created_at', '>', Carbon::now()->subHours(12)->toTimeString())->orderBy('id', 'desc')->limit(50)->offset($i*50)->get();
            $this->processArticles($articles);
        }
    }

    private function oldData() {
        $articles = Article::whereDate('updated_at', '<', Carbon::now()->subDay(7)->toDateString())->orderBy( 'id', 'asc')->limit(50)->get();
        $this->processArticles($articles, true);
    }

    private function recentData() {
        $articles = Article::whereDate('updated_at', '<', Carbon::now()->subHours(6)->toDateString())->whereDate('created_at', '>', Carbon::now()->subDay(7)->toDateString())->orderBy( 'id', 'asc')->limit(50)->get();
        $this->processArticles($articles);
    }

    private function processArticles($articles, $old = false) {
        $ids = [];
        foreach ($articles as $article) {
            $parsed = parse_url($article->link);
            $ids[] = $parsed['scheme']. '://'. $parsed['host']. $parsed['path'];
        }
        $token = \Facebook::getDefaultAccessToken();
        if ($old) {
            $token  = env('FACEBOOK_APP_ID2') . '|' . env('FACEBOOK_APP_SECRET2');
        }
        try {
            $data = \Facebook::get('?ids=' . json_encode($ids) . '&fields=engagement', $token);
        }
        catch (FacebookSDKException $e) {
            $this->line($e->getMessage());
        }
        $data = $data->getDecodedBody();
        foreach ($data as $key => $fields) {
            $article = Article::where('link', 'LIKE', '%'.$key.'%')->first();
            $article->fb_comments = $fields['engagement']['comment_count'];
            $article->fb_shares = $fields['engagement']['share_count'];
            $article->fb_reactions = $fields['engagement']['reaction_count'];
            $article->virality_absolute = $article->fb_reactions * 1 + $article->fb_comments*1.5 + $article->fb_shares * 2;
            $article->virality_normalized = $article->portal->virality?($article->virality_absolute/$article->portal->virality)*100:1;
            $article->save();
        }
    }
}
