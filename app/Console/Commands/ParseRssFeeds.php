<?php

namespace App\Console\Commands;

use App\DatabaseModels\Article;
use App\DatabaseModels\Source;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ParseRssFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twain:rss';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse RSS feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sources = Source::where('type', 'rss')->with(['portal'])->get();
        foreach ($sources as $source) {
            $this->line($source->url);
        	$feed = \Feeds::make($source->url);
        	foreach ($feed->get_items() as $item) {
                $parsed = parse_url($item->get_link());
                $link = $parsed['scheme']. '://'. $parsed['host']. $parsed['path'];
        		$article = Article::where('link', $link)->whereDate('created_at', '>', Carbon::now()->subDays(7))->count();
        		if (!$article) {
        			$article = Article::create([
        				'title' => $item->get_title(),
				        'description' => mb_substr(strip_tags($item->get_description()), 0, 255),
				        'image' => $item->get_enclosure(0)->get_link(),
				        'link' => $link
			        ]);
        			$article->portal()->associate( $source->portal->id);
        			$article->save();
		        }
	        }
        }
    }
}
