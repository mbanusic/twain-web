<?php

namespace App\Console\Commands;

use App\DatabaseModels\Category;
use App\DatabaseModels\Topic;
use App\Repositories\ArticleRepository;
use Illuminate\Console\Command;

class CacheWarmer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twain:cache:warmup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache warm up';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ArticleRepository $articleRepository)
    {
        //main feed
        $articleRepository->getOldArticles(true);
        $this->line('Main feed');
        $categories = Category::all();
        foreach ($categories as $category) {
            $articleRepository->getCategory($category->id, true);
            $this->line('Category '. $category->name);
        }
        $topics = Topic::all();
        foreach ($topics as $topic) {
            $articleRepository->search($topic->title, true);
            $this->line('Topic '.$topic->title);
        }
        $this->line('Cache warmed up');
    }
}
