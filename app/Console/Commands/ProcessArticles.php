<?php

namespace App\Console\Commands;

use App\DatabaseModels\Article;
use Illuminate\Console\Command;

class ProcessArticles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twain:process {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process articles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	if ($this->option('all')) {
    		$articles = Article::orderBy('id', 'desc')->get();
	    }
	    else {
		    $articles = Article::whereNull( 'processed' )->orderBy( 'id', 'desc')->limit(100)->get();
	    }
	    foreach ($articles as $article) {
	        $this->line($article->id);
        	$r = $article->processArticle();
        	if ($r !== true) {
        	    $this->line($r);
            }
        }
    }
}
