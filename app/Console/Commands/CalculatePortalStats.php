<?php

namespace App\Console\Commands;

use App\DatabaseModels\Article;
use App\DatabaseModels\Portal;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CalculatePortalStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twain:portal:stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate all portal stats';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //frequency
	    $portals = Portal::all();
	    foreach ($portals as $portal) {
		    $num_articles = Article::where( 'portal_id', $portal->id )->whereDate('created_at', '>', Carbon::now()->subDay(7)->toDateString())->count();
		    $portal->frequency = intval($num_articles/7);

		    $sum_virality = DB::table( 'articles')->where('portal_id', $portal->id)->whereDate( 'created_at', '>', Carbon::now()->subDay(7)->toDateString())->sum( 'virality_absolute');
		    $num_virality = DB::table( 'articles')->where('portal_id', $portal->id)->whereDate( 'created_at', '>', Carbon::now()->subDay(7)->toDateString())->count();
		    if ($num_virality) {
			    $portal->virality = $sum_virality / $num_virality;
		    }
		    else {
		    	$portal->virality = 1;
		    }
		    $portal->save();
	    }
    }
}
