<?php

namespace App\Console;

use App\Console\Commands\CacheWarmer;
use App\Console\Commands\CalculatePortalStats;
use App\Console\Commands\LoadHomepages;
use App\Console\Commands\ParseRssFeeds;
use App\Console\Commands\PullFacebookData;
use App\Console\Commands\PullFacebookDataCustom;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        LoadHomepages::class,
	    ParseRssFeeds::class,
	    PullFacebookData::class,
	    PullFacebookDataCustom::class,
	    CalculatePortalStats::class,
        CacheWarmer::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('twain:rss')
                  ->cron('*/20 * * * *')->withoutOverlapping();
         $schedule->command('twain:process')
                  ->cron('*/10 * * * *')->withoutOverlapping();
         $schedule->command( 'twain:pull:facebook recent')
	         ->everyFiveMinutes()->withoutOverlapping();
        $schedule->command( 'twain:pull:facebook old')
            ->cron('*/20 * * * *')->withoutOverlapping();
        $schedule->command( 'twain:pull:facebook new')
            ->everyTenMinutes()->withoutOverlapping();
         $schedule->command( 'twain:portal:stats')
	         ->daily();
         $schedule->command('twain:cache:warmup')->everyFiveMinutes()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
