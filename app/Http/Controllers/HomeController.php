<?php

namespace App\Http\Controllers;

use App\DatabaseModels\Article;
use App\Repositories\ArticleRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{

	private $article;

	public function __construct(ArticleRepository $article) {
		$this->article = $article;
		//$this->middleware('auth')->except( ['index']);
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$posts = $this->article->getArticles();
        return view('home', ['articles' => $posts]);
    }

    public function search(Request $request) {
    	$posts  = $this->article->search( $request->input('term') );
    	$request->flash();
	    return view('home', ['articles' => $posts]);
    }
}
