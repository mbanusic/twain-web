<?php

namespace App\Http\Controllers;

use App\DatabaseModels\Article;
use App\DatabaseModels\Category;
use App\Repositories\ArticleRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
	private $article;

	public function __construct(ArticleRepository $article) {
		$this->article = $article;
		$this->middleware('auth')->except( ['index', 'show']);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', ['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $validatedData = $request->validate([
		    'name' => 'required',
		    'image' => 'file',
            'keywords' => 'present',
            'order' => 'integer'
	    ]);
	    if (isset($validatedData['image'])) {
		    $file                   = $request->image->store( 'images', 'public' );
		    $validatedData['image'] = $file;
	    }
	    else {
	    	$validatedData['image'] = ' ';
	    }
	    $inclusive = [];
	    foreach (explode(',', $validatedData['keywords']) as $word) {
	    	$inclusive[] = trim($word);
	    }
	    $validatedData['keywords'] = ['inclusive' => $inclusive];
    	$category = Category::create($validatedData);
        return redirect()->route('categories.show', ['category' => $category]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
    	$category->load(['articles' => function($query) {
    		$query->limit(10)->orderBy('id', 'desc');
	    }]);
	    return view('categories.show', ['category'=>$category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
	    return view('categories.edit', ['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
	    $validatedData = $request->validate([
		    'name' => 'required',
		    'image' => 'file',
            'keywords' => 'present',
            'order' => 'integer'
	    ]);
	    if (isset($validatedData['image'])) {
		    $file                   = $request->image->store( 'images', 'public' );
		    $validatedData['image'] = $file;
	    }
	    $inclusive = [];
	    foreach (explode(',', $validatedData['keywords']) as $word) {
		    $inclusive[] = trim($word);
	    }
	    $validatedData['keywords'] = ['inclusive' => $inclusive];
	    $category->update($validatedData);
	    return redirect()->route('categories.edit', ['category'=>$category]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
    	$category->delete();
        return redirect()->route('categories.index');
    }
}
