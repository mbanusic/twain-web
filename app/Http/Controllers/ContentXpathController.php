<?php

namespace App\Http\Controllers;

use App\DatabaseModels\ContentXpath;
use App\DatabaseModels\Portal;
use Illuminate\Http\Request;

class ContentXpathController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('xpaths.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Portal $portal)
    {
        $validatedData = $request->validate([
        	'xpath' => 'required',
        ]);
        $xpath = ContentXpath::create($validatedData);
		$xpath->portal()->associate( $portal);
		$xpath->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DatabaseModels\ContentXpath  $contentXpath
     * @return \Illuminate\Http\Response
     */
    public function show(ContentXpath $contentXpath)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DatabaseModels\ContentXpath  $contentXpath
     * @return \Illuminate\Http\Response
     */
    public function edit(ContentXpath $contentXpath, Portal $portal)
    {
        return view('xpaths.edit', ['portal' => $portal, 'xpath' => $contentXpath]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DatabaseModels\ContentXpath  $contentXpath
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContentXpath $contentXpath, Portal $portal)
    {
	    $validatedData = $request->validate([
		    'xpath' => 'required',
	    ]);
	    $contentXpath->update($validatedData);
	    return redirect()->route('xpaths.edit', ['portal' => $portal, 'xpath' => $contentXpath])->with( 'status', 'Xpath updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DatabaseModels\ContentXpath  $contentXpath
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContentXpath $contentXpath, Portal $portal)
    {
        $contentXpath->delete();
        return redirect()->route( 'portals.show', ['portal' => $portal]);
    }
}
