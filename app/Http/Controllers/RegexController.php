<?php

namespace App\Http\Controllers;

use App\DatabaseModels\Category;
use App\DatabaseModels\CategoryRegex;
use Illuminate\Http\Request;

class RegexController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regexes = CategoryRegex::all();
        return view('category_regex.index', ['regexes' => $regexes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$categories = Category::all();
        return view('category_regex.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
        	'regex' => 'required',
	        'description' => 'present'
        ]);
        $regex = CategoryRegex::create($validatedData);
        if ($request->has( 'categories')) {
        	$regex->categories()->attach( $request->input('categories'));
        }
        return redirect()->route('category_regex.edit', ['category_regex' => $regex]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DatabaseModels\CategoryRegex  $categoryRegex
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryRegex $categoryRegex)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DatabaseModels\CategoryRegex  $categoryRegex
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryRegex $categoryRegex)
    {
	    $categories = Category::all();
    	$categoryRegex->load('categories');
        return view('category_regex.edit', ['regex' => $categoryRegex, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DatabaseModels\CategoryRegex  $categoryRegex
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryRegex $categoryRegex)
    {
	    $validatedData = $request->validate([
		    'regex' => 'required',
		    'description' => 'present'
	    ]);
	    $categoryRegex->update($validatedData);
	    if ($request->has( 'categories')) {
		    $categoryRegex->categories()->sync( $request->input('categories'));
	    }
	    else {
	    	$categoryRegex->categories()->detach();
	    }

	    return redirect()->view('category_regex', ['regex' => $categoryRegex]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DatabaseModels\CategoryRegex  $categoryRegex
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryRegex $categoryRegex)
    {
        $categoryRegex->delete();
    }
}
