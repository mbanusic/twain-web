<?php

namespace App\Http\Controllers;

use App\DatabaseModels\Article;
use App\DatabaseModels\Category;
use App\DatabaseModels\Portal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PortalController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth')->except( ['index', 'show']);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portals = Portal::all();
        return view('portals.index', ['portals' => $portals]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('portals.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $validatedData = $request->validate([
		    'name' => 'required',
		    'url' => 'required'
	    ]);
	    $portal = Portal::create($validatedData);
	    if ($request->has( 'categories')) {
		    $portal->categories()->attach( $request->input('categories'));
	    }
	    return redirect()->route('portals.edit', ['portal' => $portal])->with( 'status', 'Portal created');
    }

    /**
     * Display the specified resource.
     *
     * @param  Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function show(Portal $portal)
    {
        $portal->load(['articles' => function($query) {
        	$query->limit(10)->orderBy('id', 'desc');
        }]);

        return view('portals.show', ['portal' => $portal]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function edit(Portal $portal)
    {
    	$categories = Category::all();
        return view('portals.edit', ['portal' => $portal, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Portal $portal)
    {
	    $validatedData = $request->validate([
		    'name' => 'required',
		    'url' => 'required'
	    ]);
	    $portal->update($validatedData);
	    if ($request->has( 'categories')) {
		    $portal->categories()->sync( $request->input('categories'));
	    }
	    else {
		    $portal->categories()->detach();
	    }
	    return redirect()->route('portals.edit', ['portal' => $portal])->with( 'status', 'Portal updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portal $portal)
    {
        //
    }
}
