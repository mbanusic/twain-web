<?php

namespace App\Http\Controllers;

use App\DatabaseModels\Portal;
use App\DatabaseModels\Source;
use Illuminate\Http\Request;

class SourceController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Portal $portal)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Portal $portal)
    {
        return view('sources.create', ['portal' => $portal]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Portal $portal)
    {
    	$source = Source::create(['url' => $request->input('url'), 'type' => $request->input('type')]);
    	$portal->sources()->save($source);
	    return redirect()->route('sources.show', ['portal' => $portal, 'source' => $source]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DatabaseModels\Source  $source
     * @return \Illuminate\Http\Response
     */
    public function show(Portal $portal, Source $source)
    {
	    return view('sources.edit', ['portal' => $portal, 'source' => $source]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DatabaseModels\Source  $source
     * @return \Illuminate\Http\Response
     */
    public function edit(Portal $portal, Source $source)
    {
	    return view('sources.edit', ['portal' => $portal, 'source' => $source]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DatabaseModels\Source  $source
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Portal $portal, Source $source)
    {
    	$source->url = $request->input('url');
    	$source->type = $request->input('type');
    	$source->save();
	    return redirect()->route('sources.edit', ['portal' => $portal, 'source' => $source]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DatabaseModels\Source  $source
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portal $portal, Source $source)
    {
        $source->delete();
        return redirect()->route( 'portals.show', ['portal' => $portal]);
    }
}
