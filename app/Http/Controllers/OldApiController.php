<?php

namespace App\Http\Controllers;

use App\DatabaseModels\Category;
use App\DatabaseModels\Topic;
use App\Repositories\ArticleRepository;
use Illuminate\Http\Request;

class OldApiController extends Controller {

	private $article;

	public function __construct(ArticleRepository $article) {
		$this->article = $article;
	}

	public function main_feed() {
		return response()->json($this->article->getOldArticles());
	}

	public function categories() {
        $categories = Category::orderBy('order', 'asc')->get();
		$items = [];
		$i = 1;
		foreach ($categories as $category) {
			$items[] = $category->getOldData($i);
			$i++;
		}
		$out = [
			"status" => 1,
            "data" => $items
		];
		return response()->json($out);
	}

	public function category(Request $request) {
		$id = $request->input('id');
		return response()->json($this->article->getCategory( $id));
	}

	public function topics() {
        $topics = Topic::where('app', true)->orderBy('order', 'asc')->get();
		$data = [];
		foreach ($topics as $i => $topic) {
			$data[] = $topic->getOldData($i+1);
		}
		$out = [
			"status" => 1,
                "data" => $data
		];
		return response()->json($out);
	}

	public function topic(Topic $topic) {
		$articles = $this->article->search( $topic->query );
		$data = [];
		$i = 1;
		foreach ($articles as $article) {
			$data[] = $article->getOldDataForTopic($i);
			$i++;
		}
		$out = [
			"status" => 1,
            "data" =>  [
				"news" => $data
			]
		];
		return response()->json($out);
	}

	public function search(Request $request) {
		$search = $request->input('q');
		$articles = $this->article->search( $search );
		$data = [];
		$i = 1;
		foreach ($articles as $article) {
			$data[] = $article->getOldData($i)['items'];
			$i++;
		}
		$out = [
			"status" => 1,
			"data" => $data
		];
		return response()->json($out);
	}
}