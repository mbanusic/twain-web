<?php

namespace App\Http\Controllers;

use App\DatabaseModels\Article;
use App\DatabaseModels\Topic;
use App\Repositories\ArticleRepository;
use Illuminate\Http\Request;

class TopicController extends Controller
{
	private $article;

	public function __construct(ArticleRepository $article) {
		$this->article = $article;
		$this->middleware('auth')->except( ['index', 'show']);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::all();
        return view('topics.index', ['topics'=>$topics]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $validatedData = $request->validate([
		    'title' => 'required',
		    'overtitle' => 'required',
		    'query' => 'required',
		    'app' => 'boolean',
		    'active' => 'boolean',
		    'private' => 'boolean',
            'image' => 'file',
            'order' => 'integer'
	    ]);
	    if (!isset($validatedData['active'])) {
	    	$validatedData['active'] = true;
	    }
	    if (!isset($validatedData['private'])) {
		    $validatedData['private'] = true;
	    }
	    if (isset($validatedData['image'])) {
		    $file                   = $request->image->store( 'images', 'public' );
		    $validatedData['image'] = $file;
	    }
    	$topic = Topic::create($validatedData);
        return redirect()->route('topics.show', ['topic' => $topic]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
    	$articles = $this->article->search( $topic->query );
	    return view('topics.show', ['topic'=>$topic, 'articles' => $articles]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
	    return view('topics.edit', ['topic'=>$topic]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topic $topic)
    {
	    $validatedData = $request->validate([
		    'title' => 'required',
		    'overtitle' => 'required',
		    'query' => 'required',
		    'app' => 'boolean',
		    'active' => 'boolean',
		    'private' => 'boolean',
            'image' => 'file',
            'order' => 'integer'
	    ]);
	    if (isset($validatedData['image'])) {
		    $file                   = $request->image->store( 'images', 'public' );
		    $validatedData['image'] = $file;
	    }
	    $topic->update($validatedData);
	    return view('topics.edit', ['topic'=>$topic]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic)
    {
    	$topic->delete();
        return redirect()->route('topics.index');
    }
}
