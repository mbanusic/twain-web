<?php

namespace App\DatabaseModels;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DatabaseModels\Tag
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Tag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Tag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Tag whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Tag whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Tag extends Model {

    protected $guarded = [];
}