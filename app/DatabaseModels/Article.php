<?php

namespace App\DatabaseModels;

use Carbon\Carbon;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Database\Eloquent\Model;
use Embed\Embed;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

/**
 * App\DatabaseModels\Article
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $image
 * @property string|null $description
 * @property string|null $og_title
 * @property string|null $og_image
 * @property string|null $og_description
 * @property string|null $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\Category[] $categories
 * @property-read \App\DatabaseModels\Portal $portal
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\Tag[] $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereOgDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereOgImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereOgTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $link
 * @property int|null $portal_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article wherePortalId($value)
 * @property string|null $type
 * @property int|null $in_feed
 * @property int|null $processed
 * @property string|null $processed_when
 * @property string|null $published_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereInFeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereProcessedWhen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article wherePublishedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereType($value)
 * @property int|null $fb_reactions
 * @property int|null $fb_comments
 * @property int|null $fb_shares
 * @property float|null $virality_absolute
 * @property int|null $virality_normalized
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereFbComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereFbReactions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereFbShares($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereViralityAbsolute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Article whereViralityNormalized($value)
 */
class Article extends Model {

	protected $guarded = [];

	public function portal() {
		return $this->belongsTo( 'App\DatabaseModels\Portal', 'portal_id');
	}

	public function categories() {
		return $this->belongsToMany( 'App\DatabaseModels\Category', 'article_category');
	}

	public function tags() {
		return $this->belongsToMany( 'App\DatabaseModels\Tag', 'article_tag');
	}

	public function getOldData($sequence = 1) {
		$this->load( ['portal']);
		return [
			'id' => $this->id,
			'type' => 'article',
			'sequence' => $sequence,
			'items_ids' => null,
			'items' => [
				'id' => (string)$this->id,
				'source' => $this->portal->name,
				'article_url' => $this->link,
				'image_url' => $this->og_image,
				'title' => $this->title,
				'virality' => 1,
				'insert_time' => $this->created_at->timestamp,
				'created_at' => $this->created_at->timestamp,
				'sequence' => 101-intval($sequence)
			]
		];
	}

	public function getOldDataForTopic($sequence = 1) {
		$this->load( ['portal']);
		return [
				'id' => (string)$this->id,
				'source' => $this->portal->name,
				'article_url' => $this->link,
				'image_url' => $this->og_image,
				'title' => $this->title,
				'virality' => 1,
				'insert_time' => $this->created_at->timestamp,
				'created_at' => $this->created_at->timestamp,
				'sequence' => intval(100-$sequence)
		];
	}

	public function processArticle() {
		try {
			$info = Embed::create( $this->link );
			$this->title = $info->getTitle();
			if ( ! $this->image ) {
				$this->image = $info->getImage();
			}
			if ( ! $this->description ) {
				$this->description = strip_tags( $info->getDescription() );
			}
			$providers = $info->getProviders();
			if ( ! $this->og_image && count( $providers['opengraph']->getImagesUrls() ) ) {
				$this->og_image = $providers['opengraph']->getImagesUrls()[0];
			}
			if ($info->getPublishedTime()) {
				$this->created_at = Carbon::parse($info->getPublishedTime());
			}
			$this->getCategories();
			$this->setTags( $info->getTags());
			$this->processed = true;
			$this->processed_when = Carbon::now();
			$this->save();
			return true;
		}
		catch (\Exception $exception) {
			return $exception->getMessage();
		}
	}

	public function setTags($tags) {
		foreach ($tags as $tag) {
			$dtag = Tag::where('slug', mb_strtolower( $tag))->firstOrCreate(['name' => $tag, 'slug' => mb_strtolower( $tag)]);
			$this->tags()->attach( $dtag->id);
		}
	}

	public function getCategories() {
		$categories = [];

		//default portal categories
		if ($this->portal->categories()->count()) {
			$categories = array_merge($categories, $this->portal->categories->pluck('id')->all());
		}

		//categories by keywords
		$cats = Category::whereNotNull('keywords')->get();
		foreach ($cats as $cat) {
			foreach ($cat->keywords['inclusive'] as $word) {
				if (strpos($this->link, $word)) {
					$categories[] = $cat->id;
				}
			}
		}

		//Categories by regex
		$regexes = CategoryRegex::all();
		foreach ($regexes as $regex) {
			$matches = [];
			if (preg_match( '#'.$regex->regex.'#', $this->link, $matches)) {
				$regex->load(['categories']);
				$categories = array_merge( $categories, $regex->categories->pluck('id')->all());
			}
		}
		$categories = array_unique( $categories);
		$this->categories()->sync($categories);
	}

	public function getFacebookData($token = null) {
	    if (!$token) {
            $token = \Facebook::getDefaultAccessToken();
        }
        try {
            $data = \Facebook::get('?id=' . urlencode($this->link) . '&fields=engagement', $token);
        }
        catch (FacebookSDKException $e) {
	        dd($e->getMessage());
        }
		$data = $data->getDecodedBody();
		$this->fb_comments = $data['engagement']['comment_count'];
		$this->fb_shares = $data['engagement']['share_count'];
		$this->fb_reactions = $data['engagement']['reaction_count'];
		$this->virality_absolute = $this->fb_reactions * 1 + $this->fb_comments*1.5 + $this->fb_shares * 2;
		$this->virality_normalized = $this->portal->virality?($this->virality_absolute/$this->portal->virality)*100:1;
		$this->save();
	}

	public function getViralityNormalizedAttribute($value) {
		if (!$value) {
			return 100;
		}
		return $value;
	}

}