<?php

namespace App\DatabaseModels;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DatabaseModels\ContentXpath
 *
 * @property int $id
 * @property int|null $portal_id
 * @property string|null $xpath
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\ContentXpath whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\ContentXpath whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\ContentXpath wherePortalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\ContentXpath whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\ContentXpath whereXpath($value)
 * @mixin \Eloquent
 * @property-read \App\DatabaseModels\Portal|null $portal
 */
class ContentXpath extends Model
{
	protected $guarded = [];

	public function portal() {
		return $this->belongsTo( 'App\DatabaseModels\Portal');
	}
}
