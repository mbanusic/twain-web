<?php

namespace App\DatabaseModels;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DatabaseModels\Source
 *
 * @property int $id
 * @property int|null $portal_id
 * @property string|null $url
 * @property string|null $type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Source whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Source whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Source wherePortalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Source whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Source whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Source whereUrl($value)
 * @mixin \Eloquent
 * @property-read \App\DatabaseModels\Portal|null $portal
 */
class Source extends Model {

	protected $guarded = [];

	public function portal() {
		return $this->belongsTo( 'App\DatabaseModels\Portal');
	}
}