<?php

namespace App\DatabaseModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * App\DatabaseModels\Portal
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $favicon
 * @property string|null $url
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\Article[] $articles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\Source[] $sources
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Portal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Portal whereFavicon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Portal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Portal whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Portal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Portal whereUrl($value)
 * @mixin \Eloquent
 * @property float|null $virality
 * @property float|null $frequency
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\Category[] $categories
 * @property-read mixed $sources_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\ContentXpath[] $xpaths
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Portal whereFrequency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Portal whereVirality($value)
 */
class Portal extends Model {

	protected $guarded = [];

	protected $appends = ['sources_count', 'articles_count', 'last_article'];

	public function sources() {
		return $this->hasMany( Source::class);
	}

	public function categories() {
		return $this->belongsToMany( 'App\DatabaseModels\Category', 'category_portal');
	}

	public function articles() {
		return $this->hasMany( Article::class);
	}

	public function xpaths() {
		return $this->hasMany( 'App\DatabaseModels\ContentXpath');
	}

	public function getUrlAttribute($value) {
		if (strpos($value, 'http')===false) {
			return 'https://'.$value;
		}
		return $value;
	}

	public function getSourcesCountAttribute() {
		$count = Cache::get( 'sources_count.'.$this->id);
		if (!$count) {
			$count = $this->sources()->count();
			Cache::set( 'sources_count.'.$this->id, $count, 60);
		}
		return $count;
	}

	public function getArticlesCountAttribute() {
        $count = Cache::get( 'articles_count.'.$this->id);
        if (!$count) {
            $count = $this->articles()->count();
            Cache::set( 'articles_count.'.$this->id, $count, 60);
        }
        return $count;
    }

    public function getLastArticleAttribute() {
        $a = Cache::get( 'last_article.'.$this->id);
        if (!$a) {
            $a = $this->articles()->orderBy('id', 'desc')->limit(1)->first();
            if ($a) {
                $a = $a->created_at->toDateString();
                Cache::set( 'last_article.'.$this->id, $a, 60);
            }
            else {
                $a = 'no articles';
            }
        }
        return $a;
    }
}