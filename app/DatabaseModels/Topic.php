<?php

namespace App\DatabaseModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * App\DatabaseModels\Topic
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $overtitle
 * @property string|null $query
 * @property string|null $keywords
 * @property int $active
 * @property int $private
 * @property int $app
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic whereApp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic whereOvertitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic wherePrivate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic whereQuery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Topic whereImage($value)
 */
class Topic extends Model
{
    protected $guarded = [];

    protected $casts = [
        'order' => 'integer'
    ];

    public function getOldData($i) {
    	return [
    		"id" => (string)$this->id,
		    "sequence" => $i,
		    "title" => $this->title,
		    "previous_trending" => 0,
		    "label" => $this->overtitle,
		    "image" => $this->image,
		    "image_wide" => $this->image
	    ];
    }

    public function getImageAttribute($value) {
    	if ($value) {
		    return url( Storage::url( $value ) );
	    }

	    return '';
    }
}
