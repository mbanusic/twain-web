<?php

namespace App\DatabaseModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * App\DatabaseModels\Category
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $image
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\Category whereImage($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\CategoryRegex[] $regexes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\Tag[] $tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\Article[] $articles
 */
class Category extends Model {


	protected $guarded = [];

	protected $casts = [
        'keywords' => 'json',
        'order' => 'integer'
	];

	public function articles() {
		return $this->belongsToMany( 'App\DatabaseModels\Article', 'article_category');
	}

	public function getOldData($i=0) {
		return [
			"id" => (string) $this->id,
			"sequence" => $i,
			"title" => $this->name,
			"image" => $this->image
		];
	}

	public function regexes() {
		return $this->belongsToMany( 'App\DatabaseModels\CategoryRegex', 'category_regex_category');
	}

	public function tags() {
		return $this->belongsToMany( 'App\DatabaseModels\Tag', 'category_tag');
	}

	public function getImageAttribute($value) {
		if ($value) {
			return url( Storage::url( $value ) );
		}

		return 'http://www.deghq.com/static/media_news/images/BA801DC1-BB50-1A5C-CC84-550B1DB0CB7B.png';
	}
}