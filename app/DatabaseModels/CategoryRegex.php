<?php

namespace App\DatabaseModels;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DatabaseModels\CategoryRegex
 *
 * @property int $id
 * @property string|null $regex
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DatabaseModels\Portal[] $portals
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\CategoryRegex whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\CategoryRegex whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\CategoryRegex whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\CategoryRegex whereRegex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DatabaseModels\CategoryRegex whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CategoryRegex extends Model
{

	protected $guarded = [];

    public function portals() {
    	return $this->belongsToMany( 'App\DatabaseModels\Portal', 'category_regex_portal');
    }

    public function categories() {
    	return $this->belongsToMany( 'App\DatabaseModels\Category', 'category_regex_category');
    }
}
