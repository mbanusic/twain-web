<?php
namespace App\Repositories;

use App\DatabaseModels\Category;

interface ArticleRepository {

	function getModel();

	function getAll();

	function getById($id);

	function create(array $attributes);

	function update($id, array $attributes);

	function delete($id);

	function search($term);

	function getOldArticles();

	function getArticles($offset = 0);

	function getCategory($category);
}