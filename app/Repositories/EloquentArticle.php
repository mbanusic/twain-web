<?php

namespace App\Repositories;

use App\DatabaseModels\Article;
use App\DatabaseModels\Category;
use App\DatabaseModels\Topic;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class EloquentArticle implements ArticleRepository {

	/**
	 * @var Article $model
	 */
	private $model;

	/**
	 * EloquentTask constructor.
	 *
	 * @param Article $model
	 */
	public function __construct(Article $model)
	{
		$this->model = $model;
	}

	public function getModel() {
		return $this->model;
	}

	/**
	 * Get all articles.
	 *
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function getAll()
	{
		return $this->model->all();
	}

	/**
	 * Get article by id.
	 *
	 * @param integer $id
	 *
	 * @return App\Task
	 */
	public function getById($id)
	{
		return $this->model->find($id);
	}

	/**
	 * Create a new article.
	 *
	 * @param array $attributes
	 *
	 * @return App\Task
	 */
	public function create(array $attributes)
	{
		return $this->model->create($attributes);
	}

	/**
	 * Update a article.
	 *
	 * @param integer $id
	 * @param array $attributes
	 *
	 * @return App\Task
	 */
	public function update($id, array $attributes)
	{
		return $this->model->find($id)->update($attributes);
	}

	/**
	 * Delete a article.
	 *
	 * @param integer $id
	 *
	 * @return boolean
	 */
	public function delete($id)
	{
		return $this->model->find($id)->delete();
	}

	public function topOfTheDay() {
	    $out = Cache::get('topOfTheDay');
	    if (!$out) {
            $articles = $this->model->whereNotNull('og_image')->with(['portal'])->groupBy('portal_id')->orderBy('id', 'desc')->limit(5)->get();
            $items = [];
            foreach ($articles as $key => $article) {
                $items[] = $article->getOldData($key + 1)['items'];
            }
            $out = ["id" => 98803543,
                "type" => "topOfTheDay",
                "sequence" => 10,
                "item_ids" => null,
                "items" => $items
            ];
            Cache::put( 'topOfTheDay', $out, 60*24);
        }
        return $out;
	}

	public function trendingTopics() {
        $out = Cache::get('trendingTopics');
        if (!$out) {
            $topics = Topic::limit(5)->get();
            $items = [];
            foreach ($topics as $key => $topic) {
                $items[] = $topic->getOldData($key + 1);
            }
            $out = [
                "id" => 70113387,
                "type" => "trendingTopics",
                "sequence" => 20,
                "item_ids" => null,
                "items" => $items
            ];
            Cache::put( 'trendingTopics', $out, 60*24);
        }
        return $out;
	}

	public function needToKnow() {
        $out = Cache::get('needToKnow');
        if (!$out) {
            $articles = $this->model->whereNotNull('og_image')->with(['portal'])->groupBy('portal_id')->orderBy('id', 'desc')->limit(5)->offset(5)->get();
            $items = [];
            foreach ($articles as $key => $article) {
                $items[] = $article->getOldData($key + 1)['items'];
            }
            $out = [
                "id" => 90618520,
                "type" => "needToKnow",
                "sequence" => 30,
                "item_ids" => null,
                "items" => $items
            ];
            Cache::put( 'needToKnow', $out, 60*24);
        }
        return $out;
	}

	public function inCaseYouMissed() {
        $out = Cache::get('inCaseYouMissed');
        if (!$out) {
            $articles = $this->model->whereNotNull('og_image')->with(['portal'])->groupBy('portal_id')->orderBy('id', 'desc')->limit(5)->offset(10)->get();
            $items = [];
            foreach ($articles as $key => $article) {
                $items[] = $article->getOldData($key + 1)['items'];
            }
            $out = [
                "id" => 90618520,
                "type" => "inCaseYouMissed",
                "sequence" => 40,
                "item_ids" => null,
                "items" => $items
            ];
            Cache::put('inCaseYouMissed', $out, 60 * 24);
        }
        return $out;
	}

	public function getOldArticles($cache = false) {
		$out = Cache::get('old_articles');
		if (!$out || $cache) {
            $big_articles = $this->model->limit(100)
               /* ->whereHas('portal', function($query) {
                $query->where('frequency', '>', 15);
            })*/
                ->whereNotNull('og_image')
                ->whereDate('created_at', '>', Carbon::now()->subDay(1)->toDateString())
                ->orderBy('virality_normalized', 'desc')
                ->with(['portal'])->get();
  /*          $small_articles = $this->model->limit(50)->whereHas('portal', function($query) {
                $query->where('frequency', '<=', 15);
            })->whereNotNull('og_image')->whereDate('created_at', '>', Carbon::now()->subDay(3)->toDateString())->orderBy('virality_normalized', 'desc')->with(['portal'])->get();
*/
			$news     = [];
			$i        = 0;
			$j = 0;
			$portals = [];
            while ($big_articles->count()) {
                foreach ($big_articles as $key => $article) {
					if ( ! in_array( $article->portal_id, $portals ) ) {
						$news[] = $article->getOldData( $i+1 );
                        $big_articles->forget($key);
						$portals[] = $article->portal_id;
						$i ++;
						if ( $i == 9 ) {
							$news[] = $this->topOfTheDay();
						}
						if ( $i == 19 ) {
							$news[] = $this->trendingTopics();
						}
						if ( $i == 29 ) {
							$news[] = $this->needToKnow();
						}
						if ( $i == 39 ) {
							$news[] = $this->inCaseYouMissed();
						}
					}
					/*if ($i > 0 && $i%3 == 0) {
					    if (isset($small_articles[$j])) {
                            $news[] = $small_articles[$j]->getOldData($i + 1);
                            $i++;
                            $j++;
                        }
                    }*/
				}
				$portals = [];
			}
			$out = [
				"status" => 1,
				"data"   => [
					"news" => $news
				]
			];
			Cache::put( 'old_articles', $out, 15);
		}
		return $out;
	}

	public function search($term, $cache = false) {
        $articles = Cache::get('old_articles.search'.$term);
        if (!$articles || $cache) {
            $terms = explode(',', $term);

            $articles = $this->model
                ->where(function ($query) use ($terms) {
                    $query->where(function ($query) use ($terms) {
                        foreach ($terms as $term) {
                            $query->where('title', 'REGEXP', trim($term));
                        }
                    });
                    $query->orWhere(function ($query) use ($terms) {
                        foreach ($terms as $term) {
                            $query->where('description', 'REGEXP', trim($term));
                        }
                    });
                })
                ->whereNotNull('og_image')
                ->limit(20)->orderBy('id', 'desc')->get();

            Cache::put( 'old_articles.search'.$term, $articles, 15);
        }
        return $articles;
	}

	public function getArticles( $offset = 0 ) {
		return $this->model->orderBy( 'id', 'desc')->whereNotNull('og_image')->limit(10)->offset($offset)->get();
	}

	public function getCategory($category, $cache = false ) {
		$out = Cache::get('old_articles.category'.$category);
		if (!$out || $cache) {
			$articles = Category::find($category)->articles()->whereNotNull( 'og_image')->orderBy( 'id', 'desc' )->limit(100)->get();
			$news     = [];
			$i        = 0;
			$portals = [];
			while($articles->count()) {
				foreach ( $articles as $key => $article ) {
					if ( ! in_array( $article->portal_id, $portals ) ) {
						$news[] = $article->getOldData( $i+1 );
						$articles->forget( $key );
						$portals[] = $article->portal_id;
						$i ++;
					}
					if ( $i == 10 ) {
						$news[] = $this->topOfTheDay();
					}
					if ( $i == 20 ) {
						$news[] = $this->trendingTopics();
					}
					if ( $i == 40 ) {
						$news[] = $this->inCaseYouMissed();
					}
				}
				$portals = [];
			}
			$out = [
				"status" => 1,
				"data"   => [
					"news" => $news
				]
			];
			Cache::put( 'old_articles.category'.$category, $out, 15);
		}
		return $out;
	}
}