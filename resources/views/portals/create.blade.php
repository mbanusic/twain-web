@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                    <form action="{{ route('portals.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Enter title" value="{{ old('name') }}" name="name">
                        <small id="titleHelp" class="form-text text-muted">Title used for display.</small>
                    </div>
                    <div class="form-group">
                        <label for="homepage">Homepage</label>
                        <input type="text" class="form-control" id="homepage" aria-describedby="urlHelp" placeholder="Enter homepage" value="{{ old('url') }}" name="url">
                        <small id="urlHelp" class="form-text text-muted">Link to homepage.</small>
                    </div>
                        <div class="form-group">
                            <label>Categories</label><br>
                            @foreach ($categories as $category)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="category{{ $category->id }}" value="{{ $category->id }}" name="categories[]">
                                    <label class="form-check-label" for="category{{ $category->id }}">{{ $category->name }}</label>
                                </div>
                            @endforeach
                        </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection