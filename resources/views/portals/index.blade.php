@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @auth
                    <a class="btn btn-primary btn-lg" href="{{ route('portals.create') }}">Add portal</a><br><br>
                @endauth
                <table class="table table-striped">
                    <thead class="thead-light">
                        <tr>
                            @auth
                            <th>#</th>
                            @endauth
                            <th>Portal</th>
                            <th>Homepage</th>
                                <th>Sources</th>
                                <th>Frequency</th>
                                <th>Virality</th>
                                <th>Articles</th>
                                <th>Last</th>
                            @auth
                            <th>Actions</th>
                            @endauth
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($portals as $portal)
                        <tr>
                            @auth
                            <td><a href="{{ route('portals.edit', ['id' => $portal->id]) }}">{{ $portal->id }}</a></td>
                            @endauth
                            <td><a href="{{ route('portals.show', ['id' => $portal->id]) }}">{{ $portal->name }}</a></td>
                            <td><a href="{{ $portal->url }}" target="_blank">{{ $portal->url }}</a></td>
                                <td>{{ $portal->sources_count }}</td>
                                <td>{{ $portal->frequency }}</td>
                                <td>{{ $portal->virality }}</td>
                                <td>{{ $portal->articles_count }}</td>
                                <td>{{ $portal->last_article }}</td>
                            @auth
                            <td><a class="btn btn-primary btn-sm" href="{{ route('portals.edit', ['id' => $portal->id]) }}">Edit</a></td>
                            @endauth
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection