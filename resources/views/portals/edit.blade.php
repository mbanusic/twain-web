@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                    <form action="{{ route('portals.update', ['portal' => $portal]) }}" method="post">
                    @csrf
                        @method('put')
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Enter title" value="{{ $portal->name }}" name="name">
                        <small id="titleHelp" class="form-text text-muted">Title used for display.</small>
                    </div>
                    <div class="form-group">
                        <label for="homepage">Homepage</label>
                        <input type="text" class="form-control" id="homepage" aria-describedby="urlHelp" placeholder="Enter homepage" value="{{ $portal->url }}" name="url">
                        <small id="urlHelp" class="form-text text-muted">Link to homepage.</small>
                    </div>
                        <div class="form-group">
                            <label>Categories</label><br>
                            @foreach ($categories as $category)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="category{{ $category->id }}" value="{{ $category->id }}" name="categories[]" {{ $portal->categories->contains($category->id)?'checked':'' }}>
                                    <label class="form-check-label" for="category{{ $category->id }}">{{ $category->name }}</label>
                                </div>
                            @endforeach
                        </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                    <br><br>
                    <h2>Sources</h2>
                    <a class="btn btn-primary" href="{{ route('sources.create', ['portal'=>$portal]) }}">Add Source</a><br><br>
                    <table class="table table-striped">
                    <thead class="thead-light">
                        <tr>
                            <th>Source</th>
                            <th>Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($portal->sources as $source)
                        <tr>
                            <td>{{ $source->url }}</td>
                            <td>{{ $source->type }}</td>
                            <td><a class="btn btn-sm btn-primary" href="{{ route('sources.edit', ['portal' => $portal, 'source' => $source]) }}">Edit</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                    <br><br>
                    <h2>Content Xpaths</h2>
                    <a class="btn btn-primary" href="{{ route('xpaths.create', ['portal'=>$portal]) }}">Add Xpath</a><br><br>
                    <table class="table table-striped">
                        <thead class="thead-light">
                        <tr>
                            <th>Xpath</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($portal->xpaths as $xpath)
                            <tr>
                                <td>{{ $xpath->xpath }}</td>
                                <td><a class="btn btn-sm btn-primary" href="{{ route('xpaths.edit', ['portal' => $portal, 'xpath' => $xpath]) }}">Edit</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
@endsection