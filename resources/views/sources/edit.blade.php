@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <h2>Add source for {{ $portal->name }}</h2>
                <form action="{{ route('sources.update', ['portal' => $portal, 'source' => $source]) }}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="link">Link</label>
                        <input type="text" class="form-control" id="link" aria-describedby="linkHelp" placeholder="Enter title" value="{{ $source->url }}" name="url">
                        <small id="linkHelp" class="form-text text-muted">Link to query</small>
                    </div>
                    <div class="form-group">
                        <label for="typeSelect">Type</label>
                    <select id="typeSelect" class="custom-select" name="type">
                        <option value="rss" {{ $source->type==='rss'?'selected':'' }}>RSS</option>
                        <option value="homepage" {{ $source->type==='homepage'?'selected':'' }}>Homepage</option>
                        <option value="category" {{ $source->type==='category'?'selected':'' }}>Category</option>
                        <option value="facebook" {{ $source->type==='facebook'?'selected':'' }}>Facebook</option>
                    </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection