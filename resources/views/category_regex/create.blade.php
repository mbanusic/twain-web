@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{ route('category_regex.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="regex">Regex</label>
                        <input type="text" class="form-control" id="regex" aria-describedby="titleHelp" placeholder="Enter regex" value="" name="regex">
                        <small id="titleHelp" class="form-text text-muted">Title used for display.</small>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description" aria-describedby="descriptionHelp" placeholder="Enter description" value="" name="description">
                        <small id="descriptionHelp" class="form-text text-muted">Description.</small>
                    </div>
                    <div class="form-group">
                        <label>Categories</label><br>
                        @foreach ($categories as $category)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="category{{ $category->id }}" value="{{ $category->id }}" name="categories[]" {{ intval($_GET['category']) === $category->id?'checked':'' }}>
                                <label class="form-check-label" for="category{{ $category->id }}">{{ $category->name }}</label>
                            </div>
                        @endforeach
                    </div>
                    <button class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection