@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <table class="table table-striped">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Regex</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($regexes as $regex)
                        <tr>
                            <td><a href="{{ route('category_regex.edit', ['id' => $regex->id]) }}">{{ $regex->id }}</a></td>
                            <td><a href="{{ route('category_regex.edit', ['id' => $regex->id]) }}">{{ $regex->name }}</a></td>
                            <td>{{ $regex->description }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection