@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <h2>Add xpath for {{ $portal->name }}</h2>
                <form action="{{ route('xpaths.update', ['portal' => $portal, 'xpath' => $xpath]) }}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="link">Xpath</label>
                        <input type="text" class="form-control" id="link" aria-describedby="linkHelp" placeholder="Enter title" value="{{ $xpath->xpath }}" name="xpath">
                        <small id="linkHelp" class="form-text text-muted">Xpath for content</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection