@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @auth
                <a href="{{ route('topics.create') }}" class="btn btn-primary btn-lg">New topic</a>
                @endauth
                <table class="table table-striped">
                    <thead class="thead-light">
                    <tr>
                        @auth
                        <th>#</th>
                        @endauth
                        <th>Topic</th>
                        <th>Order</th>
                        <th>In App</th>
                        @auth
                            <th>Actions</th>
                        @endauth
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($topics as $topic)
                        <tr>
                            @auth
                            <td><a href="{{ route('topics.edit', ['id' => $topic->id]) }}">{{ $topic->id }}</a></td>
                            @endauth
                            <td><a href="{{ route('topics.show', ['id' => $topic->id]) }}">{{ $topic->title }}</a></td>
                            <td>{{ $topic->order }}</td>
                            <td>{{ $topic->app?'Yes':'No' }}</td>
                            @auth
                                <td><a class="btn btn-primary btn-sm" href="{{ route('topics.edit', ['id' => $topic->id]) }}">Edit</a></td>
                            @endauth
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection