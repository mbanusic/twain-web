@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{ route('topics.update', ['topic' => $topic]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Enter title" value="{{ $topic->title }}" name="title">
                        <small id="titleHelp" class="form-text text-muted">Title used for display.</small>
                    </div>
                    <div class="form-group">
                        <label for="overtitle">Overtitle</label>
                        <input type="text" class="form-control" id="overtitle" aria-describedby="overtitleHelp" placeholder="Enter overtitle" value="{{ $topic->overtitle }}" name="overtitle">
                        <small id="overtitleHelp" class="form-text text-muted">Overtitle used for display.</small>
                    </div>
                    <div class="form-group">
                        <label for="overtitle">Query</label>
                        <input type="text" class="form-control" id="query" aria-describedby="queryHelp" placeholder="Enter query" value="{{ $topic->query }}" name="query">
                        <small id="queryHelp" class="form-text text-muted">Query used for selecting items.</small>
                    </div>
                    <div class="form-group">
                        <label for="order">Order</label>
                        <input type="number" step="1" min="0" class="form-control" id="order"
                               aria-describedby="orderHelp" placeholder="Order" value="{{ $topic->order }}"
                               name="order">
                        <small id="orderHelp" class="form-text text-muted">Ordering in app</small>
                    </div>
                    <div class="form-group">
                        <input class="form-check-input" type="checkbox" value="1" name="app" id="in_app" {{ $topic->app?'checked':'' }}>
                        <label class="form-check-label" for="in_app">
                            In App
                        </label>
                    </div>
                    <div class="form-group">
                        @if($topic->image)
                            <img src="{{ $topic->image }}" class="col-md-12">
                        @endif
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" name="image" accept="image/*">
                            <label class="custom-file-label" for="image">Topic image</label>
                        </div>
                    </div>
                    <button class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection