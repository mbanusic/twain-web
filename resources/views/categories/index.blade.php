@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @auth
                <a href="{{ route('categories.create') }}" class="btn btn-primary btn-lg">New category</a><br><br>
                @endauth
                <table class="table table-striped">
                    <thead class="thead-light">
                    <tr>
                        @auth
                        <th>#</th>
                        @endauth
                        <th>Category</th>
                        <th>Order</th>
                        @auth
                        <th>Actions</th>
                        @endauth
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            @auth
                            <td><a href="{{ route('categories.edit', ['id' => $category->id]) }}">{{ $category->id }}</a></td>
                            @endauth
                            <td><a href="{{ route('categories.show', ['id' => $category->id]) }}">{{ $category->name }}</a></td>
                            <td>{{ $category->order }}</td>
                            @auth
                                    <td><a class="btn btn-primary btn-sm" href="{{ route('categories.edit', ['id' => $category->id]) }}">Edit</a></td>
                            @endauth
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection