@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                    @auth
                        <h2><a href="{{ route('categories.edit', ['category' => $category]) }}">{{ $category->title }}</a></h2>
                    @elseauth
                        <h2>{{ $category->title }}</h2>
                    @endauth

                @foreach($category->articles as $article)
                    <div class="card">
                        <div class="card-header"><a href="{{ $article->link }}" target="_blank">{{ $article->title }}</a>
                            <span class="float-right">{{ $article->virality_normalized }} / {{  $article->virality_absolute }}</span>
                            @auth
                                <a class="btn btn-primary btn-sm float-right" href="{{ route('articles.show', ['article' => $article]) }}">Details</a>
                            @endauth
                        </div>
                        <a href="{{ $article->link }}" target="_blank"><img class="card-img-top" src="{{ $article->image }}"></a>
                        <div class="card-body">
                            <p class="card-text">{{ $article->description }}</p>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection