@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{ route('categories.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Enter title" value="" name="name">
                        <small id="titleHelp" class="form-text text-muted">Title used for display.</small>
                    </div>
                    <div class="form-group">
                        <label for="order">Order</label>
                        <input type="number" step="1" min="0" class="form-control" id="order"
                               aria-describedby="orderHelp" placeholder="Order" value="" name="order">
                        <small id="orderHelp" class="form-text text-muted">Ordering in app</small>
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" name="image" accept="image/*">
                            <label class="custom-file-label" for="image">Category image</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="keywords">Keywords</label>
                        <input type="text" class="form-control" id="keywords" aria-describedby="wordsHelp" placeholder="Enter keywords" value="" name="keywords">
                        <small id="wordsHelp" class="form-text text-muted">Comma separated keywords to match in URL.</small>
                    </div>
                    <button class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection