@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
                <div class="card">
                    <div class="card-header"><a href="{{ $article->link }}" target="_blank">{{ $article->title }}</a></div>
                    <a href="{{ $article->link }}" target="_blank"><img class="card-img-top" src="{{ $article->og_image }}"></a>
                    <div class="card-body">
                        <p class="card-text">{{ $article->description }}</p>
                    </div>
                    <div class="card-body">
                        <h3>Tags</h3>
                        <ul>
                        @foreach ($article->tags as $tag)
                            <li>{{ $tag->name }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>

        </div>
    </div>
</div>
@endsection
