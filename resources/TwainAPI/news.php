<?php
array (
  0 => 
  array (
    'name' => 'wsj.com',
    'url' => 'wsj.com',
  ),
  1 => 
  array (
    'name' => 'thedailybeast.com/',
    'url' => 'thedailybeast.com/',
  ),
  2 => 
  array (
    'name' => 'vice.com/en_us',
    'url' => 'vice.com/en_us',
  ),
  3 => 
  array (
    'name' => 'theverge.com/',
    'url' => 'theverge.com/',
  ),
  4 => 
  array (
    'name' => 'breitbart.com/',
    'url' => 'breitbart.com/',
  ),
  5 => 
  array (
    'name' => 'vox.com/',
    'url' => 'vox.com/',
  ),
  6 => 
  array (
    'name' => 'fivethirtyeight.com/',
    'url' => 'fivethirtyeight.com/',
  ),
  7 => 
  array (
    'name' => 'talkingpointsmemo.com/',
    'url' => 'talkingpointsmemo.com/',
  ),
  8 => 
  array (
    'name' => 'nytimes.com/',
    'url' => 'nytimes.com/',
  ),
  9 => 
  array (
    'name' => 'slate.com/',
    'url' => 'slate.com/',
  ),
  10 => 
  array (
    'name' => 'mic.com/',
    'url' => 'mic.com/',
  ),
  11 => 
  array (
    'name' => 'nymag.com/',
    'url' => 'nymag.com/',
  ),
  12 => 
  array (
    'name' => 'businessinsider.com/',
    'url' => 'businessinsider.com/',
  ),
  13 => 
  array (
    'name' => 'recode.net/',
    'url' => 'recode.net/',
  ),
  14 => 
  array (
    'name' => 'bloomberg.com/',
    'url' => 'bloomberg.com/',
  ),
  15 => 
  array (
    'name' => 'vanityfair.com/',
    'url' => 'vanityfair.com/',
  ),
  16 => 
  array (
    'name' => 'economist.com',
    'url' => 'economist.com',
  ),
  17 => 
  array (
    'name' => 'wired.com/',
    'url' => 'wired.com/',
  ),
  18 => 
  array (
    'name' => 'huffingtonpost.com/',
    'url' => 'huffingtonpost.com/',
  ),
  19 => 
  array (
    'name' => 'newyorker.com/',
    'url' => 'newyorker.com/',
  ),
  20 => 
  array (
    'name' => 'washingtonpost.com',
    'url' => 'washingtonpost.com',
  ),
  21 => 
  array (
    'name' => 'washingtontimes.com',
    'url' => 'washingtontimes.com',
  ),
  22 => 
  array (
    'name' => 'avclub.com/',
    'url' => 'avclub.com/',
  ),
  23 => 
  array (
    'name' => 'foxnews.com',
    'url' => 'foxnews.com',
  ),
  24 => 
  array (
    'name' => 'bostonglobe.com/',
    'url' => 'bostonglobe.com/',
  ),
  25 => 
  array (
    'name' => 'nydailynews.com',
    'url' => 'nydailynews.com',
  ),
  26 => 
  array (
    'name' => 'rollingstone.com',
    'url' => 'rollingstone.com',
  ),
  27 => 
  array (
    'name' => 'fusion.net',
    'url' => 'fusion.net',
  ),
  28 => 
  array (
    'name' => 'gizmodo.com',
    'url' => 'gizmodo.com',
  ),
  29 => 
  array (
    'name' => 'qz.com',
    'url' => 'qz.com',
  ),
  30 => 
  array (
    'name' => 'theguardian.com',
    'url' => 'theguardian.com',
  ),
  31 => 
  array (
    'name' => 'fortune.com',
    'url' => 'fortune.com',
  ),
  32 => 
  array (
    'name' => 'thedodo.com',
    'url' => 'thedodo.com',
  ),
  33 => 
  array (
    'name' => 'bgr.com',
    'url' => 'bgr.com',
  ),
  34 => 
  array (
    'name' => 'boingboing.net',
    'url' => 'boingboing.net',
  ),
  35 => 
  array (
    'name' => 'upworthy.com',
    'url' => 'upworthy.com',
  ),
  36 => 
  array (
    'name' => 'theweek.com',
    'url' => 'theweek.com',
  ),
  37 => 
  array (
    'name' => 'newsweek.com',
    'url' => 'newsweek.com',
  ),
  38 => 
  array (
    'name' => 'propublica.org',
    'url' => 'propublica.org',
  ),
  39 => 
  array (
    'name' => 'abcnews.go.com',
    'url' => 'abcnews.go.com',
  ),
  40 => 
  array (
    'name' => 'medium.com',
    'url' => 'medium.com',
  ),
  41 => 
  array (
    'name' => 'lifehacker.com',
    'url' => 'lifehacker.com',
  ),
  42 => 
  array (
    'name' => 'time.com',
    'url' => 'time.com',
  ),
  43 => 
  array (
    'name' => 'vulture.com',
    'url' => 'vulture.com',
  ),
  44 => 
  array (
    'name' => 'nymag.com',
    'url' => 'nymag.com',
  ),
  45 => 
  array (
    'name' => 'inc.com',
    'url' => 'inc.com',
  ),
  46 => 
  array (
    'name' => 'elitedaily.com',
    'url' => 'elitedaily.com',
  ),
  47 => 
  array (
    'name' => 'latimes.com',
    'url' => 'latimes.com',
  ),
  48 => 
  array (
    'name' => 'mashable.com',
    'url' => 'mashable.com',
  ),
  49 => 
  array (
    'name' => 'cbsnews.com',
    'url' => 'cbsnews.com',
  ),
  50 => 
  array (
    'name' => 'nbcnews.com',
    'url' => 'nbcnews.com',
  ),
  51 => 
  array (
    'name' => 'npr.org',
    'url' => 'npr.org',
  ),
  52 => 
  array (
    'name' => 'msnbc.com',
    'url' => 'msnbc.com',
  ),
  53 => 
  array (
    'name' => 'observer.com',
    'url' => 'observer.com',
  ),
  54 => 
  array (
    'name' => 'nationalgeographic.com',
    'url' => 'nationalgeographic.com',
  ),
  55 => 
  array (
    'name' => 'texasmonthly.com',
    'url' => 'texasmonthly.com',
  ),
  56 => 
  array (
    'name' => 'lamag.com',
    'url' => 'lamag.com',
  ),
  57 => 
  array (
    'name' => 'smithsonianmag.com',
    'url' => 'smithsonianmag.com',
  ),
  58 => 
  array (
    'name' => 'foreignpolicy.com',
    'url' => 'foreignpolicy.com',
  ),
  59 => 
  array (
    'name' => 'clickhole.com',
    'url' => 'clickhole.com',
  ),
  60 => 
  array (
    'name' => 'thenation.com',
    'url' => 'thenation.com',
  ),
  61 => 
  array (
    'name' => 'newrepublic.com',
    'url' => 'newrepublic.com',
  ),
  62 => 
  array (
    'name' => 'salon.com',
    'url' => 'salon.com',
  ),
  63 => 
  array (
    'name' => 'weeklystandard.com',
    'url' => 'weeklystandard.com',
  ),
  64 => 
  array (
    'name' => 'chicagotribune.com',
    'url' => 'chicagotribune.com',
  ),
  65 => 
  array (
    'name' => 'fastcompany.com',
    'url' => 'fastcompany.com',
  ),
  66 => 
  array (
    'name' => 'news.vice.com',
    'url' => 'news.vice.com',
  ),
  67 => 
  array (
    'name' => 'gq.com/?us_site=y',
    'url' => 'gq.com/?us_site=y',
  ),
  68 => 
  array (
    'name' => 'theringer.com',
    'url' => 'theringer.com',
  ),
  69 => 
  array (
    'name' => 'bleacherreport.com/',
    'url' => 'bleacherreport.com/',
  ),
  70 => 
  array (
    'name' => 'deadspin.com/',
    'url' => 'deadspin.com/',
  ),
  71 => 
  array (
    'name' => 'refinery29.com',
    'url' => 'refinery29.com',
  ),
  72 => 
  array (
    'name' => 'arstechnica.com',
    'url' => 'arstechnica.com',
  ),
  73 => 
  array (
    'name' => 'daringfireball.net',
    'url' => 'daringfireball.net',
  ),
  74 => 
  array (
    'name' => '9to5mac.com',
    'url' => '9to5mac.com',
  ),
  75 => 
  array (
    'name' => 'eater.com',
    'url' => 'eater.com',
  ),
  76 => 
  array (
    'name' => 'polygon.com',
    'url' => 'polygon.com',
  ),
  77 => 
  array (
    'name' => 'jalopnik.com',
    'url' => 'jalopnik.com',
  ),
  78 => 
  array (
    'name' => 'stratechery.com',
    'url' => 'stratechery.com',
  ),
  79 => 
  array (
    'name' => 'sbnation.com',
    'url' => 'sbnation.com',
  ),
  80 => 
  array (
    'name' => 'radaronline.com',
    'url' => 'radaronline.com',
  ),
  81 => 
  array (
    'name' => 'tmz.com',
    'url' => 'tmz.com',
  ),
  82 => 
  array (
    'name' => 'thefader.com',
    'url' => 'thefader.com',
  ),
  83 => 
  array (
    'name' => 'espn.com',
    'url' => 'espn.com',
  ),
  84 => 
  array (
    'name' => 'popsci.com',
    'url' => 'popsci.com',
  ),
  85 => 
  array (
    'name' => 'politico.com',
    'url' => 'politico.com',
  ),
  86 => 
  array (
    'name' => 'lennyletter.com/style/',
    'url' => 'lennyletter.com/style/',
  ),
  87 => 
  array (
    'name' => 'themarshallproject.org',
    'url' => 'themarshallproject.org',
  ),
  88 => 
  array (
    'name' => 'harpers.org',
    'url' => 'harpers.org',
  ),
  89 => 
  array (
    'name' => 'mtv.com/news/',
    'url' => 'mtv.com/news/',
  ),
  90 => 
  array (
    'name' => 'harpersbazaar.com',
    'url' => 'harpersbazaar.com',
  ),
  91 => 
  array (
    'name' => 'jezebel.com',
    'url' => 'jezebel.com',
  ),
  92 => 
  array (
    'name' => 'aeon.co',
    'url' => 'aeon.co',
  ),
  93 => 
  array (
    'name' => 'esquire.com',
    'url' => 'esquire.com',
  ),
  94 => 
  array (
    'name' => 'theintercept.com',
    'url' => 'theintercept.com',
  ),
  95 => 
  array (
    'name' => 'luckypeach.com',
    'url' => 'luckypeach.com',
  ),
  96 => 
  array (
    'name' => 'backchannel.com',
    'url' => 'backchannel.com',
  ),
  97 => 
  array (
    'name' => 'hbr.org',
    'url' => 'hbr.org',
  ),
  98 => 
  array (
    'name' => 'entrepreneur.com',
    'url' => 'entrepreneur.com',
  ),
  99 => 
  array (
    'name' => 'cinemablend.com',
    'url' => 'cinemablend.com',
  ),
  100 => 
  array (
    'name' => 'sfchronicle.com',
    'url' => 'sfchronicle.com',
  ),
  101 => 
  array (
    'name' => 'greatbigstory.com',
    'url' => 'greatbigstory.com',
  ),
  102 => 
  array (
    'name' => 'cnbc.com',
    'url' => 'cnbc.com',
  ),
  103 => 
  array (
    'name' => 'ew.com',
    'url' => 'ew.com',
  ),
  104 => 
  array (
    'name' => 'thefrisky.com',
    'url' => 'thefrisky.com',
  ),
  105 => 
  array (
    'name' => 'people.com/',
    'url' => 'people.com/',
  ),
  106 => 
  array (
    'name' => 'inverse.com',
    'url' => 'inverse.com',
  ),
  107 => 
  array (
    'name' => 'cbr.com',
    'url' => 'cbr.com',
  ),
  108 => 
  array (
    'name' => 'theouthousers.com',
    'url' => 'theouthousers.com',
  ),
  109 => 
  array (
    'name' => 'bustle.com',
    'url' => 'bustle.com',
  ),
  110 => 
  array (
    'name' => 'complex.com',
    'url' => 'complex.com',
  ),
  111 => 
  array (
    'name' => 'pitchfork.com',
    'url' => 'pitchfork.com',
  ),
  112 => 
  array (
    'name' => 'univision.com/univision-news',
    'url' => 'univision.com/univision-news',
  ),
  113 => 
  array (
    'name' => 'hazlitt.net',
    'url' => 'hazlitt.net',
  ),
  114 => 
  array (
    'name' => 'racked.com',
    'url' => 'racked.com',
  ),
  115 => 
  array (
    'name' => 'curbed.com',
    'url' => 'curbed.com',
  ),
  116 => 
  array (
    'name' => 'chicago.suntimes.com',
    'url' => 'chicago.suntimes.com',
  ),
  117 => 
  array (
    'name' => 'miamiherald.com',
    'url' => 'miamiherald.com',
  ),
  118 => 
  array (
    'name' => 'insideedition.com',
    'url' => 'insideedition.com',
  ),
  119 => 
  array (
    'name' => 'xobenzo.com',
    'url' => 'xobenzo.com',
  ),
  120 => 
  array (
    'name' => 'providr.com',
    'url' => 'providr.com',
  ),
  121 => 
  array (
    'name' => 'aplus.com',
    'url' => 'aplus.com',
  ),
  122 => 
  array (
    'name' => 'allwomenstalk.com',
    'url' => 'allwomenstalk.com',
  ),
  123 => 
  array (
    'name' => 'theladbible.com',
    'url' => 'theladbible.com',
  ),
  124 => 
  array (
    'name' => 'waitbutwhy.com',
    'url' => 'waitbutwhy.com',
  ),
  125 => 
  array (
    'name' => 'theatlantic.com/world/',
    'url' => 'theatlantic.com/world/',
  ),
  126 => 
  array (
    'name' => 'consumerreports.org/cro/index.htm',
    'url' => 'consumerreports.org/cro/index.htm',
  ),
  127 => 
  array (
    'name' => 'motherjones.com',
    'url' => 'motherjones.com',
  ),
  128 => 
  array (
    'name' => 'cnn.com',
    'url' => 'cnn.com',
  ),
  129 => 
  array (
    'name' => 'ibtimes.com',
    'url' => 'ibtimes.com',
  ),
  130 => 
  array (
    'name' => 'buzzfeed.com',
    'url' => 'buzzfeed.com',
  ),
  131 => 
  array (
    'name' => 'theoutline.com',
    'url' => 'theoutline.com',
  ),
  132 => 
  array (
    'name' => 'littlethings.com',
    'url' => 'littlethings.com',
  ),
  133 => 
  array (
    'name' => 'dailydot.com',
    'url' => 'dailydot.com',
  ),
  134 => 
  array (
    'name' => 'thinkprogress.org',
    'url' => 'thinkprogress.org',
  ),
  135 => 
  array (
    'name' => 'dailywire.com',
    'url' => 'dailywire.com',
  ),
  136 => 
  array (
    'name' => 'http://conservativetribune.com',
    'url' => 'http://conservativetribune.com',
  ),
  137 => 
  array (
    'name' => 'https://www.theroot.com',
    'url' => 'https://www.theroot.com',
  ),
  138 => 
  array (
    'name' => 'http://www.visualcapitalist.com',
    'url' => 'http://www.visualcapitalist.com',
  ),
  139 => 
  array (
    'name' => 'http://www.allthatsfab.com',
    'url' => 'http://www.allthatsfab.com',
  ),
  140 => 
  array (
    'name' => 'https://www.ted.com',
    'url' => 'https://www.ted.com',
  ),
  141 => 
  array (
    'name' => 'https://medium.com/the-mission',
    'url' => 'https://medium.com/the-mission',
  ),
  142 => 
  array (
    'name' => 'http://hackernoon.com',
    'url' => 'http://hackernoon.com',
  ),
  143 => 
  array (
    'name' => 'https://www.thestreet.com',
    'url' => 'https://www.thestreet.com',
  ),
  144 => 
  array (
    'name' => 'http://www.valuewalk.com',
    'url' => 'http://www.valuewalk.com',
  ),
  145 => 
  array (
    'name' => 'https://washingtonmonthly.com',
    'url' => 'https://washingtonmonthly.com',
  ),
  146 => 
  array (
    'name' => 'https://www.texastribune.org',
    'url' => 'https://www.texastribune.org',
  ),
  147 => 
  array (
    'name' => 'https://www.nationalaffairs.com',
    'url' => 'https://www.nationalaffairs.com',
  ),
  148 => 
  array (
    'name' => 'https://democracyjournal.org',
    'url' => 'https://democracyjournal.org',
  ),
  149 => 
  array (
    'name' => 'https://www.lawfareblog.com',
    'url' => 'https://www.lawfareblog.com',
  ),
  150 => 
  array (
    'name' => 'https://khn.org',
    'url' => 'https://khn.org',
  ),
  151 => 
  array (
    'name' => 'https://www.statnews.com',
    'url' => 'https://www.statnews.com',
  ),
  152 => 
  array (
    'name' => 'https://www.insidehighered.com',
    'url' => 'https://www.insidehighered.com',
  ),
  153 => 
  array (
    'name' => 'http://nautil.us',
    'url' => 'http://nautil.us',
  ),
  154 => 
  array (
    'name' => 'https://www.newscientist.com',
    'url' => 'https://www.newscientist.com',
  ),
  155 => 
  array (
    'name' => 'https://www.quantamagazine.org',
    'url' => 'https://www.quantamagazine.org',
  ),
  156 => 
  array (
    'name' => 'https://newsnetwork.mayoclinic.org',
    'url' => 'https://newsnetwork.mayoclinic.org',
  ),
  157 => 
  array (
    'name' => 'https://www.citylab.com',
    'url' => 'https://www.citylab.com',
  ),
  158 => 
  array (
    'name' => 'http://www.sciencealert.com',
    'url' => 'http://www.sciencealert.com',
  ),
  159 => 
  array (
    'name' => 'http://www.iflscience.com',
    'url' => 'http://www.iflscience.com',
  ),
  160 => 
  array (
    'name' => 'https://wattsupwiththat.com',
    'url' => 'https://wattsupwiththat.com',
  ),
  161 => 
  array (
    'name' => 'https://www.scientificamerican.com',
    'url' => 'https://www.scientificamerican.com',
  ),
  162 => 
  array (
    'name' => 'http://discovermagazine.com',
    'url' => 'http://discovermagazine.com',
  ),
  163 => 
  array (
    'name' => 'https://www.insidescience.org',
    'url' => 'https://www.insidescience.org',
  ),
  164 => 
  array (
    'name' => 'https://psmag.com',
    'url' => 'https://psmag.com',
  ),
  165 => 
  array (
    'name' => 'https://earther.com',
    'url' => 'https://earther.com',
  ),
  166 => 
  array (
    'name' => 'https://splinternews.com',
    'url' => 'https://splinternews.com',
  ),
  167 => 
  array (
    'name' => 'https://io9.gizmodo.com',
    'url' => 'https://io9.gizmodo.com',
  ),
  168 => 
  array (
    'name' => 'https://thetakeout.com',
    'url' => 'https://thetakeout.com',
  ),
  169 => 
  array (
    'name' => 'https://kotaku.com',
    'url' => 'https://kotaku.com',
  ),
  170 => 
  array (
    'name' => 'https://lifehacker.com',
    'url' => 'https://lifehacker.com',
  ),
  171 => 
  array (
    'name' => 'http://www.comingsoon.net',
    'url' => 'http://www.comingsoon.net',
  ),
  172 => 
  array (
    'name' => 'https://moviepilot.com',
    'url' => 'https://moviepilot.com',
  ),
  173 => 
  array (
    'name' => 'https://www.theatlantic.com',
    'url' => 'https://www.theatlantic.com',
  ),
  174 => 
  array (
    'name' => 'https://www.forbes.com',
    'url' => 'https://www.forbes.com',
  ),
  175 => 
  array (
    'name' => 'https://www.healthline.com',
    'url' => 'https://www.healthline.com',
  ),
  176 => 
  array (
    'name' => 'http://www.elle.com',
    'url' => 'http://www.elle.com',
  ),
  177 => 
  array (
    'name' => 'http://www.bestproducts.com',
    'url' => 'http://www.bestproducts.com',
  ),
  178 => 
  array (
    'name' => 'https://www.popsugar.com',
    'url' => 'https://www.popsugar.com',
  ),
  179 => 
  array (
    'name' => 'https://www.thecut.com',
    'url' => 'https://www.thecut.com',
  ),
  180 => 
  array (
    'name' => 'http://www.townandcountrymag.com',
    'url' => 'http://www.townandcountrymag.com',
  ),
  181 => 
  array (
    'name' => 'https://www.bonappetit.com',
    'url' => 'https://www.bonappetit.com',
  ),
  182 => 
  array (
    'name' => 'https://townhall.com',
    'url' => 'https://townhall.com',
  ),
  183 => 
  array (
    'name' => 'https://themighty.com',
    'url' => 'https://themighty.com',
  ),
  184 => 
  array (
    'name' => 'https://venturebeat.com',
    'url' => 'https://venturebeat.com',
  ),
  185 => 
  array (
    'name' => 'https://thenextweb.com',
    'url' => 'https://thenextweb.com',
  ),
  186 => 
  array (
    'name' => 'https://www.dwell.com',
    'url' => 'https://www.dwell.com',
  ),
  187 => 
  array (
    'name' => 'https://www.realsimple.com',
    'url' => 'https://www.realsimple.com',
  ),
  188 => 
  array (
    'name' => 'http://www.elledecor.com',
    'url' => 'http://www.elledecor.com',
  ),
  189 => 
  array (
    'name' => 'http://www.indiewire.com',
    'url' => 'http://www.indiewire.com',
  ),
  190 => 
  array (
    'name' => 'http://www.nationalreview.com',
    'url' => 'http://www.nationalreview.com',
  ),
  191 => 
  array (
    'name' => 'http://thefederalist.com',
    'url' => 'http://thefederalist.com',
  ),
  192 => 
  array (
    'name' => 'http://thehayride.com',
    'url' => 'http://thehayride.com',
  ),
  193 => 
  array (
    'name' => 'https://themarketmogul.com',
    'url' => 'https://themarketmogul.com',
  ),
)