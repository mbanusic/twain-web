<?php
$html = file_get_contents( 'newsfeed.html');
$dom = new DOMDocument;
$dom->loadHTML($html);
$xpath = new DOMXPath($dom);
$urls = $xpath->query('//span[@class="inline-input-name"]');

$out = [];
foreach ($urls as $url) {
	$out[] = ['name' => trim($url->nodeValue), 'url' => trim($url->nodeValue)];
}

file_put_contents('news.php', "<?php\n".var_export($out, true));
